# TP : création d'utilisateurs et de groupes

vous avez déjà un compte (et un groupe) qui vous est personnel
(moi c'est "jpierre")

créons deux utilisateurs nommés joe et jane [sur les deux systèmes]

créons deux groupes : m2i et linux

(si vous voulez en faire plus, pas de pb)

~~~~Bash
useradd -m joe
useradd -m jane
~~~~

(-m est peut être le défaut, mais mettez le quand même : ça demande la
création du répertoire de connexion de l'utilateur)

~~~~Bash
groupadd m2i
groupadd linux
~~~~

jane sera membre des deux, joe seulement de m2i, vous des deux

~~~~Bash
usermod -a -G m2i jane
usermod -a -G linux jane
usermod -a -G m2i joe
usermod -a -G m2i jpierre
usermod -a -G linux jpierre
~~~~

vérifiez : `id joe`  et `id jane`, etc

Note : nous avons le droit d'utiliser `sudo` parce qu'il est préconfiguré
par les distributions pour permettre aux membre d'un certain groupe de
l'utiliser pour devenir _root_.

- groupe _sudo_ sous Debian GNU/Linux
- groupe _wheel_ sous Red Hat... Pour une raison historique assez curieuse...
  pourquoi une ... roue ? https://fr.wikipedia.org/wiki/Dharmachakra
  Tradition UNIX des années 70s que RH fait perdurer et Debian a abandonné.

https://xkcd.com/149/

