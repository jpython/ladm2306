# Profiter de l'authentification à clef publique avec SSH


## Vérifions que l'on peut à partir de notre système Rocky se connecter sur la Debian

~~~~Bash
$ ssh morlaix
The authenticity of host 'morlaix (10.145.39.66)' can't be established.
ED25519 key fingerprint is SHA256:qPxXejwTgB/ViTquibn9nd9ccJoRFD/OXfQST3i7574.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes 
Warning: Permanently added 'morlaix' (ED25519) to the list of known hosts.
jpierre@morlaix's password: 
...
$ exit
~~~~

L'avertissement vient du fait que c'est la premire fois que notre client ssh
reçoit la clef publique de ce système. Elle est maintenant enregistrée dans
`~/.ssh/known_hosts` et sera vérifiée lors des connexions futures.

N'oubliez pas de fermer la connexion ssh. On va faire en sorte de ne plus
avoir à saisir notre mot de passe :

~~~~Bash
$ ssh-keygen -t rsa
~~~~

Répondre par <Entrée> à toutes les questions : noms de fichiers standards,
et pas de phrase de passe pour chiffrer la clef privée.

Transférer la clef publique dans la liste de confiance pour notre compte
sur le système Debian :

~~~~Bash
$ ssh-copy-id -i ~/.ssh/id_rsa.pub  jpierre@morlaix
$ ssh morlaix
~~~~

Vous devriez être connecté sans mot de passe demandé !

Note : vous pouvez faire la même chose sous MS Windows !

- Le client SSH d'OpenSSH (le même que sous GNU/Linux) est pré-installé
- `ssh-keygen` fonctionne comme sous Linux
- Il faut copier le contenu de `C:\Users\Administrateur\.ssh\id_rsa.pub`
  à la main dans `~/.ssh/authorized_keys` du système Linux visé
  (on trouve des script powershell ou cmd sur le net qui font ça, mais
  "à la main" ça marche aussi en faisant du copier-coller)

Le client SSH peut se configurer avec un fichier `~/.ssh/config` :

~~~~
Host morlaix
  User jpierre
  Hostname 10.145.39.72

Host tunis
  User jpierre
  Hostname 10.145.39.66
~~~~

- Quel est exactement la condition qui permet, dès lors que l'on a configuré
l'authentification à clef publique comme nous venons de le faire, de se
connecter sur le système concerné ?
- Tel qu'on l'a fait qui, précisément, peut satisfaire cette condition ?

Réponse : 
La possibilité de _lire_ la clef privée `~/.ssh/id_rsa`.

Conclusion moi et aussi tous les gens qui ont les droits d'administration
système (avec su ou sudo) sur le système de _départ_ (et aussi, un éventuel
attaquant qui aurait réussi à obtenir ces droits).

Qu'est-ce qui protège l'accès tel qu'on l'a créée : droits UNIX et la
relative sécurité de GNU/Linux, la serrure de la porte de notre salle.

La bonne question est : à quoi nous sert cette clef ? Autrement dit si
quelqu'un nous la pique peut-il faire plus de chose que s'il ne l'avait
pas.

exemple: si cette clef nous sert à nous connecter aux différents systèmes
de notre organisation. Les autres admin peuvent déjà le faire. "Voler"
ma clef ne leur fournit AUCUN avantage.

exemple: si j'utilise cette clef pour m'authentifier, mettons, sur un
autre système qui est chez moi. Là on a un pb : un admin malveillant.

Dans certaines situation il convient de chiffrer par une phrase de
passe la clef privée. 

- C'est pourquoi on peut créer des paires de clef ssh multiples
- Si la clef privée est chiffrée par une phrase, elle sera demandé
 à la connextion, c'est ennuyeux. 
- Solution : un agent ssh qui tourne en mémoire (Gnome le lance à
la connextion), vous pouvez en lancer un à la main :
`eval $(ssh-agent)` et la commande `ssh-add` permet de charger
dans cet agent une clef privée donnée. 

cf. Mr Robot et Matrix (l'episode 1 Trinity utilise une faille
de SSH v1).

Pour finaliser la sécurisation de nos systèmes on _pourrait_ :

- Désactiver l'authentification par mot de passe dans `/etc/ssh/sshd_config`
  (c'est le défaut sur les plateformes de Clous IaaS)
- Réfléchir à l'intérêt ou pas de permettre la connexion en tant que `root`
  en ssh (pourquoi pas ? surtout ne pas le permettre avec un mot de passe)
- Une fois certain que `sudo` est utilisable on peut verrouiller le compte
  `root` : `sudo passwd -l root` 
  










