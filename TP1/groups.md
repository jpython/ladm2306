# Créons un répertoire où les membres du groupe "linux" pourront collaborer facilement

~~~~Bash
$ id jane
$ id jpierre
~~~~

Ok on est bien membre du groupe "linux".

Ben en fait non. Il faudrait se déloguer et se reloguer du bureau pour
que l'appartenance à ce groupe soit effective, ou bien on peut 
faire `newgrp linux` (ou se loguer en ssh localhost)

~~~~
$ sudo mkdir /srv/linux
$ sudo chgrp linux /srv/linux
$ sudo chmod g+ws /srv/linux
$ sudo newgrp linux # inutile si vous vous reloguez
$ echo echo Bonjour > /srv/linux/script_de_jp
$ ls -l /srv/linux
$ ls -l /srv/linux/
total 4
-rw-r--r--. 1 jpierre linux 13 12 juin  16:21 script_de_jp
$ chmod g+w /srv/linux/script_de_jp 
$ ls -l /srv/linux/
total 4
-rw-rw-r--. 1 jpierre linux 13 12 juin  16:21 script_de_jp
~~~~
