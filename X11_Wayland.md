# La pile graphique de GNU/Linux

## X11

Rappel : la première interface graphique "moderne" : Xerox
Alto au PARC. Ils ont eux des visiteurs, Steve Jobs et
Bill Gates.

Interfaces graphiques pour UNIX : 198x, Sunview, NeWS, qq
autres (NextStep a eu son propre système, qui est devenu
Cocoa dans mac OS) et le MIT a développé X-Window System
publié en v11 : interface graphique + protocole réseau.

X11 est devenu la norme de fait sous UNIX.

X11 a été pendant des années géré par Xorg. X11 a fait
sont temps, le code est devenu difficile à gérer, qq
problèmes de fonds, dont la sécurité. 

Xorg a décidé de tout remettre à plat et de développer
un nouveau système : Wayland.

~~~~$
$ echo $XDG_SESSION_TYPE 
wayland
~~~~

Wayland, par exemple, ne laisse pas une application obtenir
des information sur le pointeur ou le clavier si elle n'a
pas le "focus". X11 le permettait !

Exemple xeyes !

~~~~Bash
$ xeyes &
$ xedit &
~~~~

Quand le pointeur est sur une fenêtre X11 xeyes le "voit", sinon
(dans Gnome Terminal par exemple) non.

Quelques petits souci : les applis qui souhaitent capturer l'écran
entier. Bureau à distance et export d'affichage en réseau (projecteurs),
tout ne marche pas nécessairement encore avec Wayland.

Airtame distribue un boitier qui se connecte à un projecteur et
à un réseau WiFI. On installe un client airtame pour exporter
l'affichage. Gratuit, pas open source et ne marche qu'avec X11.

Pilotes libres : Intel, AMD/ATI, nVidia : pilote anciennement
propriétaires récemment libérés, pilotes libres "nouveau".

Notes : Wine (et Play On Linux) permet de faire tourner des
application Windows sous GNU/Linux y compris des jeux avec
de bonnes performance.

Testons ça :

1. Installer wine sous Debian
2. Télécharger le meilleur que Microsoft ait jamais développé : le démineur, `winmine.exe`

	http://www.minesweeper.info/downloads/WinmineXP.html

3. Essayes de le faire tourner, il y aura des chose à installer mais wine va nous le
   dire clairement
4. On peut essayer d'autres logiciels pour Windows... 
5. Quand on aura fait le TP sur Samba on pourra facilement copier des exécutables
   de MS Windows à Linux (on pourrait déjà le faire avec scp)

~~~~Bash
$ wine Winmine__XP.exe 
$ dpkg --add-architecture i386
$ sudo dpkg --add-architecture i386
$ sudo apt-get update 
$ sudo apt install wine32
$ wine Winmine__XP.exe 
$ unzip ProcessExplorer.zip 
$ wine procexp64.exe 
~~~~
