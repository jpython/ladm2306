Quelques ressources Internet supplémentaires 

UNIX : Pour aller plus loin avec la ligne de commande
Vincent Lozano
https://archives.framabook.org/unixpou-allerplusloinaveclalignedecommande/

Linux Weekly News : http://lwn.net/ 
 -> édition courante payante, gratuite au bout d'une semaine
    (voir la section Archives)

Communauté Linux Francophone : http://linuxfr.org/

Slashdot : http://slashdot.org/

Le site de la société française Bootlin : http://bootlin.fr/

Linux from scratch : installation entièrement manuelle
https://www.linuxfromscratch.org/


