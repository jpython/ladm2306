# Installation d'un logiciel à partir de son code source


1. Clonez le dépôt git : https://github.com/hills/xosview

~~~~Bash
$ git clone https://github.com/hills/xosview.git
~~~~

2. Allez dans le répertoire `xosview` regardez un peu la doc...

3. On va constuire le binaire de xosview (sans être root)

4. Installez le (là il faudra être root)

Vous aurez sans doute à résoudre des pb de dépendance pour la compilation
(et donc être root est utile pour faire des apt install ou yum install)
Sur Rocky yum provides ... sera très utile, et apt-file sur la Debian.

À vous : sur la Debian ou sur la Rocky ou sur les deux.

## Rocky (ou Red Hat ou CentOS ou ...)

~~~~Bash
$ sudo yum install git
$ git clone https://github.com/hills/xosview.git 
$ cd xosview
$ make
g++  -Wall -O3 -I. -MMD -Ilinux/  -c -o Host.o Host.cc
g++ -Wno-write-strings -Wall -O3 -I. -MMD -Ilinux/  -c -o Xrm.o Xrm.cc
Dans le fichier inclus depuis Xrm.cc:7:
Xrm.h:10:10: erreur fatale: X11/Xlib.h : Aucun fichier ou dossier de ce type
   10 | #include <X11/Xlib.h>
      |          ^~~~~~~~~~~~
compilation terminée.
make: *** [<commande interne> : Xrm.o] Erreur 1
...
$ yum provides '*/Xlib.h' # fichier d'en-tête C qui définit des fonctions présentes dans des bibliothèques
libX11-devel-1.7.0-7.el9.x86_64 : Development files for libX11
Dépôt               : appstream
Correspondances trouvées dans  :
Nom de fichier : /usr/include/X11/Xlib.h
$ sudo yum install libX11-devel
$ make
xwin.cc:4:10: erreur fatale: X11/xpm.h : Aucun fichier ou dossier de ce type
    4 | #include <X11/xpm.h>
      |          ^~~~~~~~~~~
compilation terminée.
make: *** [<commande interne> : xwin.o] Erreur 1
$ yum provides '*/xpm.h' 
libXpm-devel-3.5.13-8.el9_1.x86_64 : X.Org X11 libXpm development package
Dépôt               : appstream
Correspondances trouvées dans  :
Nom de fichier : /usr/include/X11/xpm.h
$ sudo yum install libXpm-devel
$ make
...
$ ls -l xosview
-rwxr-xr-x. 1 jpierre jpierre 311784 12 juin  22:11 xosview
$ ./xosview 
xosview: font "7x13bold" does not exist; see xlsfonts(1)
~~~~

Zut il manque une police !

~~~~Bash
$  yum search x11 | grep fonts
$  sudo yum install xorg-x11-fonts-misc # lucky guess?
$  ./xosview &
~~~~

C'est bon, il n'y a plus qu'à installer :

~~~~Bash
$ sudo make install
$ type xosview
xosview est /usr/local/bin/xosview
$ xosview &
~~~~

## Sur la Debian

La démarche est la même :

~~~~Bash
$ git clone https://github.com/hills/xosview.git
$ cd xosview/
$ ls
$ make
$ apt-file search 'X11/Xlib.h'
$ sudo apt install libx11-dev
$ make
$ apt-file search "X11/xpm.h'
$ apt-file search "X11/xpm.h"
$ sudo apt install libxpm-dev
$ make
$ ls -l xosview
$ ./xosview &
$ history 
$ sudo make install
$ type xosview 
~~~~

Missions accomplies ! Note : xosview est en réalité packagé et disponible dans les dépôts.

## Autres sources de logiciels que des sources ou des paquets de la distribution

Pour la plupart des grands logiciels populaires serveurs, développement, bureautique, etc.
on trouve son bonheur dans les dépôts des distributions.

Certains gros projets (PostgreSQL, Docker, etc.) proposent des dépôt pour la plupart
des distributions. 

Certains logiciels ne sont pas disponible dans une version récente d'une telle façon.
Quelques produits plus rares (OpenBoard), ou qui changent vite (Firefox) ou qui sont
propriétaires (MS Teams - arrêté, Airtame - partage d'écran avec un projecteur par
le réseau, qq jeux). Ces logiciels ont forte adhérence à des bibliothèques dans des
versions bien précises. Flatpak et Snap sont une proposition d'installation indépendante
des distributions de ce type de logiciel. C'est gourmand un espace, voire en ressource.

Mon sentiment :

- Snap est supporté officiellement par Canonical (Ubuntu) mais n'est pas assez riche
  et est très gourmand
- Flatpak me semble meilleurs 

Doc sur le site flathub...

Gnome propose un système d'extensions installable en allant sur le
site https://extensions.gnome.org/

On installe de moins en moins souvent des logiciels à partir des sources dans /usr/local/...








