# Construire des paquets à partir de sources

(dans les cas où le logiciel a déjà été packagé)

## Rocky, Red Hat, etc.


~~~~Bash
$ yum search Apache | grep -i server
...
httpd.x86_64 : Apache HTTP Server
...
$ yum info httpd
~~~~

Note : Red Hat a tendance à garder le nom du logiciel choisi par le projet d'origine.

Info : https://blogs.reliablepenguin.com/2009/03/21/yum-howto-download-source-packages

~~~~Bash
$ sudo yum install yum-utils
$ yumdownloader --source httpd
httpd-2.4.53-11.el9_2.5.src.rpm                                                         37 MB/s | 7.6 MB     00:00    
$ ls
httpd-2.4.53-11.el9_2.5.src.rpm
$ rpm -U httpd*rpm
Warning sur l'absence du groupe mockbuild
~~~~

Quand on crée un paquet rpm ou deb, c'est au fond un peu comme une archive
zip ou tar. Ça contient des fichiers et des informations sur leur propriétaire
et leur groupe propriétaire. Or si on est pas root il est pas évident de
créer des archives contenant root/root comme propriétaire.

C'est un peu tordu, mais ceci marche :

~~~~Bash
$ sudo yum install epel-release
$ sudo yum install mock
$ sudo useradd --system --no-create-home mockbuild
$ id mockbuild
uid=976(mockbuild) gid=974(mockbuild) groupes=974(mockbuild)
$ sudo usermod -a -G mock mockbuild
$ sudo usermod -a -G mockbuild $(whoami)
$ rpm -U httpd*rpm
~~~~

Plus de warning : c'est plus propre.

~~~~Bash
$ ls ~/rpmbuild/
SOURCES  SPECS
$ tree ~/rpmbuild
$ cd ~/rpmbuild/SPECS
$ vi httpd.spec
~~~~

On personnalise le fichier : 

~~~~
Release:           11%{?dist}.5.jp 
~~~~

On bidouille un peu, c'est notre boulot : on remplace
dans httpd.spec lua-devel par compat-lua-devel.

~~~~Bash
$ sudo yum-builddep httpd.spec
$ rm ~/rpmbuld/RPMS/*/*
$ rpmbuild -bb httpd.spec
...
$ ls ~/rpmbuild/RPMS/*/
...
plein de rpms !
$ rm ~/rpmbuild/RPMS/*/*debuginfo*
$ sudo yum localinstall ~/rpmbuild/RPMS/*/*rpm
$ sudo systemctl start httpd
$ sudo systemctl enable httpd
~~~~

Note : la politique de Red Hat est ne pas lancer/activer automatique les
services réseau installés par des paquet. Debian si.

## Et sur Debian, Ubuntu, etc.

~~~~Bash
$ apt search Apache | grep -C 3 -i serv
...
apache2/stable 2.4.57-2 amd64
  Serveur HTTP Apache
...
$ apt show apache2
... c'est bien lui
$ cd devel
$ apt source apache2
$ cd apac<tab>
~~~~

Note : Debian a tendance à renommer les noms des logiciels avec une logique
implacable. Le nom usuel ("Apache2"), ensuite ce sera le nom du paquet, le
nom du binaire, le nom du répertoire de configuration.

~~~~Bash
$ sudo apt install devscript
$ export EMAIL=jp@xiasma.fr
$ dpkg-buildpckage -us -uc
...
$ cd ..
$ rm *dpgsym*
$ sudo dpkg -i *deb
$ sudo apt -f install # (si installation incomplète)
~~~~

Note : ici on construit des paquets non-signés. Il serait préférable de
créer une paire de clefs privée/publique avec gpg et ajouter la clef
publique dans les configurations des postes où on deployera cette
version.

Conclusion : on a installé sur nos deux systèmes le serveur Web Apache 2
en reconstruisant les paquets binaires à partir des sources.

Rien de plus, en pratique, que de faire : apt install apache2 ou yum install httpd.


