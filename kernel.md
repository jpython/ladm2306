# Le noyau Linux

Site officiel : www.kernel.org
Balade dans les sources :  https://elixir.bootlin.com/linux/latest/source

make menuconfig / make gconfig / make xconfig pour configurer

on peut partir de la conf de notre distribution : /boot/config-*

on peut produire des binaire à installer à l'arrache (bof)
on peut produire un paquet rpm ou deb (mieux)

Depuis qq annnée il y a un point délicat : Secure Boot
-> les PC ont dans leur firmware une fonctionnalité reposant sur
un chipset (TPM) qui fait que seul un noyau d'OS signé par
une clef privée validée par XXX (en pratique c'est Microsoft)

Vous pouvez mettre votre clef à la place.

Les distributions ont fait signer par Microsoft, pour un prix
symbolique, leurs propres clefs. Donc ça passe...

Trois choix :

- utiliser des outils pour que votre clef soit dans TPM
- désactivé secure boot (ce que je fais systematiquement : voir conf firmware)
  (j'ai aussi désactivé complétement : management à distance)
  (note il faut alors désactiver la signature du noyau dans la configuration
  de sa compilation)
- se contenter des noyaux fournis par notre distrbution (en prod c'est ce
  que fait, hors cas particuliers : embarqué, calcul HPC)

amha : Secure Boot est une idée foireuse depuis le début. Ça m'a rappelé "Trusted
Computing" il y a 12 ou 13 ans. cf. vidéo "https://www.youtube.com/watch?v=s7WDbnHlc1E")

remarque : Windows 11 impose TPM2 et ne s'installerait pas avec TPM1.

sur la question de Management Engine : https://lwn.net/Articles/738649/

https://www.tomshardware.fr/incroyable-lettre-ouverte-du-createur-de-minix-a-intel-vous-auriez-pu-me-prevenir/

https://www.cs.cmu.edu/~rdriley/487/papers/Thompson_1984_ReflectionsonTrustingTrust.pdf

## Quelques commandes d'examen et de gestion du noyau et des modules

~~~~Bash
$ sudo dmesg | less
$ lsmod
$ modprobe module
$ modprobe -r module
$ sudo lshw | less
$ lsusb
$ lspci -v
$ lsbt
~~~~

